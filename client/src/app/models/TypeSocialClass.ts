export enum TypeSocialClass {
  FAMILY_INCOME_A1 = 'A1',
  FAMILY_INCOME_A2 = 'A2',
  FAMILY_INCOME_B = 'B',
  FAMILY_INCOME_C = 'C',
  FAMILY_INCOME_D = 'D',
  FAMILY_INCOME_E = 'E',
  FAMILY_INCOME_F = 'F',
  EMPTY = 'Não definido'
}

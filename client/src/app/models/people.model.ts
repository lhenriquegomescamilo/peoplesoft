export interface People {
  code: number;
  name: string;
  salary: number;
  typeSocialClass: string;
}

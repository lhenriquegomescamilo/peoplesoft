import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {TemplatePeopleComponent} from './template-people/template-people.component';

const routes: Routes = [
  {path: '', component: TemplatePeopleComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class PeopleRoutingModule {
}

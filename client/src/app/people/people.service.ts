import {Injectable} from '@angular/core';
import {People} from '../models/people.model';
import {HttpClient, HttpClientModule, HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PeopleService {

  private static readonly OPTIONS = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(
    private http: HttpClient
  ) {
  }

  create(people: People) {
    return this.http
      .post(`${environment.baseUrl}/peoples`, people, PeopleService.OPTIONS) as Observable<People>;
  }

  retrieveAll(): Observable<People[]> {
    return this.http
      .get(`${environment.baseUrl}/peoples`, PeopleService.OPTIONS) as Observable<People[]>;
  }
}

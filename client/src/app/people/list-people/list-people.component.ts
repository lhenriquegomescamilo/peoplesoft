import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {People} from '../../models/people.model';
import {Observable} from 'rxjs';
import {PeopleService} from '../people.service';
import {flatMap, map, mergeMap, switchMap, take, tap} from 'rxjs/operators';
import {TypeSocialClass} from '../../models/TypeSocialClass';

@Component({
  selector: 'app-list-people',
  templateUrl: './list-people.component.html',
  styleUrls: ['./list-people.component.scss']
})
export class ListPeopleComponent implements OnInit {

  displayedColumns: string[] = ['code', 'name', 'salary', 'typeSocialClass'];
  dataSource = [];


  constructor(
    private peopleService: PeopleService
  ) {
  }

  @Input()
  set newPeople(people: People) {
    this.peopleService.retrieveAll()
      .pipe(
        take(1),
        map((peoples: People[]) => peoples.map(p => ({
          ...p,
          typeSocialClass: TypeSocialClass[p.typeSocialClass]
        })))
      ).subscribe(peoples => this.dataSource = peoples);
  }

  ngOnInit() {
    this.peopleService.retrieveAll().pipe(
      take(1),
      tap(data => console.log(data)),
      map((peoples: People[]) => peoples.map(p => ({...p, typeSocialClass: TypeSocialClass[p.typeSocialClass]}))),
      tap(peoples => this.dataSource = peoples)
    ).subscribe();
  }
}

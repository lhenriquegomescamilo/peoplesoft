import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreatePeopleComponent} from './create-people/create-people.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule, MatCardModule, MatInputModule, MatProgressBarModule, MatTableModule} from '@angular/material';
import {PeopleRoutingModule} from './people-routing.module';
import {TemplatePeopleComponent} from './template-people/template-people.component';
import {ListPeopleComponent} from './list-people/list-people.component';
import {PeopleService} from './people.service';
import {HttpClientModule} from '@angular/common/http';


@NgModule({
  declarations: [
    CreatePeopleComponent,
    TemplatePeopleComponent,
    ListPeopleComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    PeopleRoutingModule,
    MatInputModule,
    MatTableModule,
    MatCardModule,
    MatButtonModule,
    HttpClientModule,
    MatProgressBarModule
  ],
  providers: [PeopleService]
})
export class PeopleModule {
}

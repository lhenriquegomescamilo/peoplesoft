import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PeopleService} from '../people.service';
import {debounceTime, flatMap, map, take, tap} from 'rxjs/operators';
import {People} from '../../models/people.model';

@Component({
  selector: 'app-create-people',
  templateUrl: './create-people.component.html',
  styleUrls: ['./create-people.component.scss']
})
export class CreatePeopleComponent implements OnInit {
  private formGroup: FormGroup;

  onSubmitted = false;

  @Output()
  readonly eventEmitter = new EventEmitter<People>();

  constructor(
    private formBuilder: FormBuilder,
    private peopleService: PeopleService
  ) {
  }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(2)]],
      salary: [0, [Validators.required, Validators.minLength(3)]]
    });
  }

  onSubmit() {
    const people = this.formGroup.value as People;
    if (this.formGroup.valid) {
      this.onSubmitted = true;
      this.peopleService.create(people)
        .pipe(
          take(1),
          tap(() => this.onSubmitted = !this.onSubmitted),
          debounceTime(500),
          tap(() => this.formGroup.reset({name: '', salary: 0}, {emitEvent: false})),
        )
        .subscribe((peopleCreated: People) => this.eventEmitter.emit(peopleCreated));
    }
  }

  resetForm() {
    this.formGroup.reset({name: '', salary: 0}, {emitEvent: false, onlySelf: true});
  }


}

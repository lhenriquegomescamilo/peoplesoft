import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplatePeopleComponent } from './template-people.component';

describe('TemplatePeopleComponent', () => {
  let component: TemplatePeopleComponent;
  let fixture: ComponentFixture<TemplatePeopleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemplatePeopleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplatePeopleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

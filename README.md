#### Passos para reprodução

1. Primeiro necessário criar o exectável da aplicação com o comando
```bash
$ ./gradlew assembleServerAndClient
```

2. Se tudo der certo, execute o seguinte comando:
```bash
$ java -jar ./build/libs/peoplesoft-0.1.jar
``` 

3. Com comando abaixo irá executar o servidor, no seguinte endereço:
```bash
http://localhost:8000
```
4. O projeto nesse link possui um front-end escrito em Angular e um backend realizado em java

#### Observação
Esse projeto foi desenvolvimendo para resolver os exercício prático na unidade II: 
**Aula 06**  - Sobrescritas Importantes, Empacotamento e Acesso U3 (GEE12007) URL


#### Arquitetura do projeto
* Esse projeto segue a arquitetura [baseada em componentes](https://en.wikipedia.org/wiki/Component-based_software_engineering)
```bash
src/main/java
└── peoplesoft
    ├── Application.java
    ├── exceptions
    │   └── AlreadyException.java
    ├── interfaces
    │   └── ToModel.java
    └── people
        ├── PeopleController.java
        ├── PeopleService.java
        └── models
            ├── People.java
            ├── PeoplePayload.java
            └── TypeSocialClass.java
```
* O grande diferencial é que todo a regra de negócio de cada componentes estão contidades no mesmo pacote.
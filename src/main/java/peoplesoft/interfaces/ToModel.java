package peoplesoft.interfaces;

public interface ToModel<T> {
	T toModel();
}

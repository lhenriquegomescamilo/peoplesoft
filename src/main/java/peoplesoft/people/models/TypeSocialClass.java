package peoplesoft.people.models;

import java.util.stream.Stream;

enum TypeSocialClass {

	FAMILY_INCOME_A1("A1") {
		@Override
		public Boolean rangeOfSalary(Integer salary) {
			return salary >= 14_400;
		}
	},
	FAMILY_INCOME_A2("A2") {
		@Override
		public Boolean rangeOfSalary(Integer salary) {
			return salary >= 8_100 && salary < 14_400;
		}
	},
	FAMILY_INCOME_B("B") {
		@Override
		public Boolean rangeOfSalary(Integer salary) {
			return salary >= 4_600 && salary < 8_100;
		}
	},
	FAMILY_INCOME_C("C") {
		@Override
		public Boolean rangeOfSalary(Integer salary) {
			return salary >= 2_300 && salary < 4_600;
		}
	},
	FAMILY_INCOME_D("D") {
		@Override
		public Boolean rangeOfSalary(Integer salary) {
			return salary >= 1_400 && salary < 2_300;
		}
	},
	FAMILY_INCOME_E("E") {
		@Override
		public Boolean rangeOfSalary(Integer salary) {
			return salary >= 400 && salary < 950;
		}
	},
	FAMILY_INCOME_F("F") {
		@Override
		public Boolean rangeOfSalary(Integer salary) {
			return salary <= 400;
		}
	},
	EMPTY("") {
		@Override
		public Boolean rangeOfSalary(Integer salary) {
			return Boolean.FALSE;
		}
	};

	private String typeSocialClass;


	TypeSocialClass(String typeSocialClass) {
		this.typeSocialClass = typeSocialClass;
	}

	public abstract Boolean rangeOfSalary(final Integer salary);


	public static TypeSocialClass findByRangeOfSalary(final Integer salary) {
		return Stream.of(TypeSocialClass.values())
				.filter(socialClass -> socialClass.rangeOfSalary(salary))
				.findFirst().orElse(TypeSocialClass.EMPTY);
	}

	public String getTypeSocialClass() {
		return typeSocialClass;
	}
}


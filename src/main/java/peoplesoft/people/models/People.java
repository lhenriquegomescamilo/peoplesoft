package peoplesoft.people.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class People {
	private static Integer counterCode = 0;
	private Integer code;
	private String name;
	private Integer salary;
	private TypeSocialClass typeSocialClass;

	public People() {
		counterCode++;
		code = counterCode;
	}

	public People(Integer code, String name, Integer salary, TypeSocialClass typeSocialClass) {
		this.code = code;
		this.name = name;
		this.salary = salary;
		this.typeSocialClass = typeSocialClass;
		counterCode++;
		this.code = counterCode;

	}

	People defineSocialClassBySalary() {
		this.typeSocialClass = TypeSocialClass.findByRangeOfSalary(this.salary);
		return this;
	}

}

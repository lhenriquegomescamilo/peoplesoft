package peoplesoft.people.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import peoplesoft.interfaces.ToModel;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PeoplePayload implements ToModel<People> {

	@NotBlank
	private String name;

	@NotNull
	@Positive
	private Integer salary;


	@Override
	@JsonIgnore
	public People toModel() {
		return People.builder()
				.name(getName())
				.salary(getSalary())
				.build()
				.defineSocialClassBySalary();
	}
}
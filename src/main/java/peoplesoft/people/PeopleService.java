package peoplesoft.people;


import peoplesoft.exceptions.AlreadyException;
import peoplesoft.people.models.People;

import javax.inject.Singleton;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;

@Singleton
class PeopleService {

	private Queue<People> peoplesDatabase = new LinkedList<>();

	Collection<People> getPeoples() {
		return peoplesDatabase;
	}

	People createPeople(final People people) throws AlreadyException {
		if (peoplesDatabase.contains(people))
			throw new AlreadyException(String.format("The people %s already exists, the code is %s", people.getName(), people.getCode()));
		peoplesDatabase.add(people);
		return people;
	}
}

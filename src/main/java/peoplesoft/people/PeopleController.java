package peoplesoft.people;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.MutableHttpResponse;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;
import lombok.extern.slf4j.Slf4j;
import peoplesoft.exceptions.AlreadyException;
import peoplesoft.people.models.People;
import peoplesoft.people.models.PeoplePayload;

import javax.validation.Valid;
import java.util.Collection;

@Controller("/peoples")
@Slf4j
public class PeopleController {

	private PeopleService peopleService;

	public PeopleController(final PeopleService peopleService) {
		this.peopleService = peopleService;
	}

	@Get(produces = MediaType.APPLICATION_JSON)
	public HttpResponse<Collection<People>> getPeoples() {
		final Collection<People> peoples = peopleService.getPeoples();
		return HttpResponse.ok(peoples);
	}

	@Post(processes = MediaType.APPLICATION_JSON)
	public MutableHttpResponse<Object> createPerson(@Body @Valid PeoplePayload peoplePayload) {
		try {
			final People people = peopleService.createPeople(peoplePayload.toModel());
			log.info(String.format("Creating the people with name %s and code is %s", people.getName(), people.getCode()));
			return HttpResponse.created(people);
		} catch (AlreadyException e) {
			return HttpResponse.badRequest().body(e.getLocalizedMessage());
		}
	}
}

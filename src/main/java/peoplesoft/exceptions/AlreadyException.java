package peoplesoft.exceptions;

public class AlreadyException extends Exception {
	public AlreadyException() {
		super();
	}

	public AlreadyException(final String message) {
		super(message);
	}
}
